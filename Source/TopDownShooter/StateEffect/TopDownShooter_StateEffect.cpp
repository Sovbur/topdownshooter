// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter_StateEffect.h"
#include "TopDownShooter/Character/TopDownShooterHealthComponent.h"
#include "TopDownShooter/Interface/TopDownShooter_IGameActor.h"
#include "Kismet/GameplayStatics.h"
//#include "TimerManager.h"

bool UTopDownShooter_StateEffect::InitObject(AActor* Actor)
{	
	myActor = Actor;

	ITopDownShooter_IGameActor* myInterface = Cast<ITopDownShooter_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}


void UTopDownShooter_StateEffect::DestroyObject()
{
	myActor = nullptr;
	ITopDownShooter_IGameActor* myInterface = Cast<ITopDownShooter_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTopDownShooter_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();	
	return true;
}

void UTopDownShooter_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTopDownShooter_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTopDownShooterHealthComponent* myHealthComp = Cast<UTopDownShooterHealthComponent>(myActor->GetComponentByClass(UTopDownShooterHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTopDownShooter_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTopDownShooter_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTopDownShooter_StateEffect_ExecuteTimer::Execute, RateTime, true);
		if (ParticleEffect)
	{
		//ToDo for object with interface create func return offset, Name Bones,
		//ToDo random init effect with aviable array (for)
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}
	return true;
}

void UTopDownShooter_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTopDownShooter_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTopDownShooterHealthComponent* myHealthComp = Cast<UTopDownShooterHealthComponent>(myActor->GetComponentByClass(UTopDownShooterHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
