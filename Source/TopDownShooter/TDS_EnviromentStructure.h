// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TopDownShooter/Interface/TopDownShooter_IGameActor.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TopDownShooter/StateEffect/TopDownShooter_StateEffect.h"
#include "TDS_EnviromentStructure.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDS_EnviromentStructure : public AActor, public ITopDownShooter_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnviromentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	EPhysicalSurface GetSurfaceType() override;

	TArray<UTopDownShooter_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTopDownShooter_StateEffect* RemoveEffect) override;
	void AddEffect(UTopDownShooter_StateEffect* newEffect) override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTopDownShooter_StateEffect*> Effects;
};
